﻿using System.IO;
using System.Text;
using System.Web.Hosting;

namespace CustomRazorEngine.Core
{
    public class CustomVirtualFile : VirtualFile
    {
        private readonly string _template;

        public CustomVirtualFile(string virtualPath, string template) : base ( virtualPath )
    {
            this._template = template;
        }

        public override System.IO.Stream Open()
        {
            return new MemoryStream(Encoding.ASCII.GetBytes(_template));
        }

    }
}