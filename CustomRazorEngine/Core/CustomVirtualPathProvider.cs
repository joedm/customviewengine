﻿using System;
using System.Collections;
using System.Linq;
using System.Web.Caching;
using System.Web.Hosting;
using System.Web.UI;

namespace CustomRazorEngine.Core
{
    public class CustomVirtualPathProvider : VirtualPathProvider
    {
        public override bool FileExists(string virtualPath)
        {
            var page = FindPage(virtualPath);
            if (page == null)
            {
                return base.FileExists(virtualPath);
            }
            else
            {
                return true;
            }
        }

        public override VirtualFile GetFile(string virtualPath)
        {
            var page = FindPage(virtualPath);
            return page == null 
                ? base.GetFile(virtualPath) 
                : new CustomVirtualFile(virtualPath, FindPage(virtualPath));
        }

        public override bool DirectoryExists(string virtualDir)
        {
            return base.DirectoryExists(virtualDir);
        }

        public override VirtualDirectory GetDirectory(string virtualDir)
        {
            return base.GetDirectory(virtualDir);
        }

        public override CacheDependency GetCacheDependency(string virtualPath, IEnumerable virtualPathDependencies, DateTime utcStart)
        {
            return null;
        }

        public override string GetFileHash(string virtualPath, IEnumerable virtualPathDependencies)
        {
            return Guid.NewGuid().ToString();
        }

        private string FindPage(string virtualPath)
        {
            // do database lookup or whatever here to get the string content of the view.
            return virtualPath.Contains("test") && virtualPath.EndsWith(".cshtml") ? "my custom view page2. @Test()" : null;
        }
    }
}