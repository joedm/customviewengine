﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CustomRazorEngine.Core;

namespace CustomRazorEngine
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            HostingEnvironment.RegisterVirtualPathProvider(new CustomVirtualPathProvider());

            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            ////Remove All View Engine including Webform and Razor
            //ViewEngines.Engines.Clear();

            ////Register Razor View Engine
            //ViewEngines.Engines.Add(new CustomViewEngine());
        }
    }
}
