﻿using System.Web;
using System.Web.Mvc;

namespace CustomRazorEngine.Views
{
    public abstract class CustomWebViewPage<TModel> : WebViewPage<TModel>
    {
        protected IHtmlString Test()
        {
            return new HtmlString("this is a custom helpers output");
        }
    }
    public abstract class CustomWebViewPage : CustomWebViewPage<dynamic>
    {
        protected CustomWebViewPage() : base()
        {
        }
    }
}